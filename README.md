# Relax CLI

This project is our CLI, currently public due to gitlab limitations in regard to installing npm modules with private repositories.

## Setup

1. npm install
```shell
npm install
```

2. run cli
```shell
./bin/relax-cli <command>
```