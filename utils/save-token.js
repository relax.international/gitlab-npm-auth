const projectIds = [
  "11722453", // gitlab-npm-auth
  "11713623", // sails-utils
  "11584423", // schemas
  "12382189", // identity-node-client
  "12382323", // identity-react-client
  "12382677", // locale-node-client
  "12382718", // locale-react-client
  "12608420", // scraper-sails-client
];

/**
 * Generates .npmrc content based on the projectIds provided above.
 * @param {*} access_token 
 */
function getNpmrcContent(access_token) {
  var npmContent = `@relax.international:registry=https://gitlab.com/api/v4/packages/npm/
  //gitlab.com/api/v4/packages/npm/:_authToken=${access_token}\n`;
  projectIds.forEach(function(projectId) {
    npmContent += `//gitlab.com/api/v4/projects/${projectId}/packages/npm/:_authToken=${access_token}\n`;
  });
  return npmContent;
}
/**
 * Saves access token in a .npmrc file in the dest folder. Current folder by default.
 * @param {*} access_token 
 */
module.exports = function SaveAccessToken(access_token, dest) {
  const fs = require('fs');
  var path = require('path');
  var absolutePath = path.resolve(dest);
  fs.writeFile(absolutePath, getNpmrcContent(access_token), function(err) {
      if(err) {
          return console.log(err);
      }

      console.log(`🎉 .npmrc saved to '${absolutePath}'!`);
  }); 
}
  