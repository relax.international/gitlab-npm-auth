const keychain = require('keychain');
module.exports = function(options){
  return new Promise(function(resolve, reject) {
    console.log("Saving to keychain with following data:");
    console.log({
      username: options.account,
      password: "<refreshtoken_hidden>",
      service: options.service,
    })

    if(process.platform === "darwin") {
      keychain.setPassword({ 
        account: options.account,
        password: options.password,
        service: options.service,
      }, function(err) {
        if(err) {
          reject(err);
        } else {
          resolve();
        }
      });
    } else {
      resolve();
    }
  });
}