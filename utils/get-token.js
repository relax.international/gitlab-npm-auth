
const fetch = require('node-fetch');
module.exports = function(body){
  return fetch('https://gitlab.com/oauth/token', {
    method: 'POST',
    body: body,
    headers: { 'Content-Type': 'application/json' },
  }).then(function(res) {
    return res.json();
  });
}