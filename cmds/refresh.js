const keychain = require('keychain');
const saveAccessToken = require('../utils/save-token');
const getToken = require('../utils/get-token');
const setPassword = require('../utils/set-password');

module.exports = function(args) {
  const username = args.username || args.u || "relaxinternational";
  const service = args.service || args.s || "relax-gitlab";
  const dest = args.dest || args.d || "./.npmrc";

  console.log("Authenticating to GitLab with account " + "\x1b[32m'" + username + "'\x1b[0m");
  keychain.getPassword({ 
    account: username,
    service: service,
  }, function(err, refresh_token) {
    if(err) {
      console.error("\x1b[31mError refreshing auth:", err, "\x1b[0m");
      return;
    }

    getToken(JSON.stringify({
      grant_type: 'refresh_token',
      refresh_token: refresh_token,
    })).then(function(json) {
      if(json.access_token) {
        saveAccessToken(json.access_token, dest);
      }
      if(json.refresh_token) {
        setPassword({ 
          account: username,
          password: json.refresh_token,
          service: service,
        });
      }
    }).catch(function(err) {
      console.error("\x1b[31mError authenticating to gitlab:", err, "\x1b[0m")
    });
  });
};