const minimist = require('minimist');
const saveAccessToken = require('../utils/save-token');
const getToken = require('../utils/get-token');
const setPassword = require('../utils/set-password');

module.exports = function(args) {
  const username = args.username || args.u || "relaxinternational";
  const service = args.service || args.s || "relax-gitlab";
  const password = args.password || args.p;
  const dest = args.dest || args.d || "./.npmrc";

  if(!password) {
    console.error("\x1b[31mYou need to provide --password, -p argument.\x1b[0m")
    return;
  }

  getToken(JSON.stringify({
    grant_type: 'password',
    username: username,
    password: password,
  })).then(function(json) {
    if(json.access_token) {
      saveAccessToken(json.access_token, dest);
    }
    if(json.refresh_token) {    
      setPassword({ 
        account: username || "relaxinternational",
        password: json.refresh_token,
        service: service || "relax-gitlab",
      }).then(function() {
        console.log("🎉 Login succes!");
      }).catch(function(err) { 
        console.error("\x1b[31mError setting password:", err, "\x1b[0m");
      });
    }
  }).catch(function(err) {
    console.error("\x1b[31mError authenticating to gitlab:", err, "\x1b[0m")
  });
};