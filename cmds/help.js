const menus = {
  main: `
    relax-cli [command] <options>

    login .............. login to gitlab
    refresh ............ refresh access token gained from login, and save .npmrc
    version ............ show package version
    help ............... show help menu for a command`,

  login: `
    relax-cli login --password <password> [--username <username>] [--service <servicename>]
    `,

  refresh: `
    relax-cli refresh [--username <username>] [--service <servicename>]
    `,
}

module.exports = (args) => {
  const subCmd = args._[0] === 'help'
    ? args._[1]
    : args._[0]

  console.log(menus[subCmd] || menus.main)
}